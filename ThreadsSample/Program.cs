﻿using System;
using System.Threading;

namespace ThreadsSample
{
    class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource t=new CancellationTokenSource();
            ThreadPool.QueueUserWorkItem(c=> CalculateUp(t.Token, 99999999));
            ThreadPool.QueueUserWorkItem(c=>CalculateDown(t.Token, 99999999));
            ThreadPool.QueueUserWorkItem(c => CalculateUp(t.Token, 99999999));
            ThreadPool.QueueUserWorkItem(c => CalculateDown(t.Token, 99999999));
            ThreadPool.QueueUserWorkItem(c => CalculateUp(t.Token, 99999999));
            ThreadPool.QueueUserWorkItem(c => CalculateDown(t.Token, 99999999));
            ThreadPool.QueueUserWorkItem(c => CalculateUp(t.Token, 9999));
            ThreadPool.QueueUserWorkItem(c => CalculateDown(t.Token, 9999));
            t.Token.Register(() => Console.WriteLine("Canceled"));
            Console.ReadKey();
            t.Cancel();
            Console.ReadKey();
            //780
        }
        public static void CalculateUp(CancellationToken t, int obj)
        {
            int i=0,count = obj;
            while (i<count)
            {
                if(t.IsCancellationRequested)
                {
                    Console.WriteLine("CalculateUp canceled");
                    break;
                }
                Console.WriteLine(i);
                ++i;
                //Thread.Sleep(1);
            }
        }
        public static void CalculateDown(CancellationToken t, int obj)
        {
            int i = obj,count=0;
            while (i >= count)
            {
                if (t.IsCancellationRequested)
                {
                    Console.WriteLine("CalculateDown canceled");
                    break;
                }
                Console.WriteLine(i);
                --i;
                //Thread.Sleep(1);
            }
        }
    }
}
